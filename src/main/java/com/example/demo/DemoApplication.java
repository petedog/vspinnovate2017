package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.mashape.unirest.http.*;
import com.mashape.unirest.http.exceptions.*;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import java.net.URL;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import java.io.File;

import java.util.regex.*;

@Controller
@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

    @RequestMapping("/isPeter")
    public @ResponseBody String stuff(){
            String responseBody = getJsonBody();
            System.out.println(responseBody);
            boolean isPeter = isPeter(responseBody);

            System.out.println("Booean is: "+isPeter);
            if(isPeter){
                playPeterSound();
            }else{
                playNotPeterSound();
            }
//            playSound();
            return responseBody;
    }


    private boolean isPeter(String jsonB){
        System.out.println( jsonB.contains("Peter"));
        boolean b = jsonB.contains("Peter");
        return b;
    }

    private String getJsonBody(){
        try {
            HttpResponse<String> response = Unirest.post("http://api.kairos.com/recognize")
          .header("app_id", "94a12dfc")
          .header("app_key", "b2046f192fa945aa3c970980d932f092")
          .header("content-type", "application/application-json")
          .header("cache-control", "no-cache")
          .header("postman-token", "af719d5b-0e63-20de-5e6e-3da36f4a149e")
          .body("{\n    \"image\":\"http://drive.google.com/uc?export=view&id=0B-sZrCJI5lf4ZjZ2M1kyRXVxRVU\",\n    \"gallery_name\":\"VSPInnovate2017\"\n}")
          .asString();
          return response.getBody();
        }catch(Exception e){
                e.printStackTrace();
        }
        return "Fail";
    }

    private void playSound(String pathToWave){
        try{
           File soundFile = new File(pathToWave);
           AudioInputStream audioIn = AudioSystem.getAudioInputStream(soundFile);
            Clip clip = AudioSystem.getClip();
            clip.open(audioIn);
            clip.start();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }

    }
    private void playPeterSound(){
           playSound("/Users/peteja/Downloads/peter.wav");
    }
    private void playNotPeterSound(){
           playSound("/Users/peteja/Downloads/idkyf.wav");
    }
}
